const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');

const devServer = process.env.npm_lifecycle_event === 'serve';
const devtool = process.env.npm_lifecycle_event === 'build' ? false : 'source-map';

/* global __dirname */
// se config example: https://webpack.js.org/configuration/
module.exports = {
	entry: {
		main: './assets/src/index.js',
		style: './assets/src/style.js',
	},
	mode: 'production',
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules|bower_components|.git)/,
				loader: 'babel-loader',
				options: {
					presets: ['@babel/preset-env', '@babel/preset-react']
				}
			},
			{
				test: /\.scss$/,
				use: [
					devServer ? 'style-loader' : MiniCssExtractPlugin.loader,
					{
						loader:'css-loader',
						options: {
							// sourceMap: true,
							import: false,
							url: false
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							postcssOptions: {
								plugins: [
									'postcss-flexbugs-fixes',
									[
										'postcss-preset-env',
										{
											autoprefixer: {
												flexbox: 'no-2009',
											},
											stage: 3
										}
									],
									'postcss-normalize',
								]
							}
						}
					},
					{
						loader: 'sass-loader'
					}
				]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				type: 'asset/resource'
			}
		]
	},
	devServer: {
		// port: 3000,
		//  server: {
		// 	type: 'https',
		// 	options: {
		// 		key: '/Applications/MAMP/conf/ssl/eikeland.pem',
		// 		cert: '/Applications/MAMP/conf/ssl/eikeland.pem',
		// 		ca: '/Applications/MAMP/conf/ssl/eikeland.pem'
		// 	}
		// },
		hot: true,
		static: [path.join(__dirname, '')],
		client: {
			overlay: {
				errors: true,
				warnings: false
			}
		},
		allowedHosts: 'all',
		historyApiFallback: true
	},
	watchOptions: {
		ignored: ['**/*.txt', '**/node_modules/', '**/.git*', '**/package*.json', '**/.eslintrc.*'],
	},
	devtool: devtool,
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'assets/js'),
		assetModuleFilename: '../img/[name][ext]'
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: (data) => {
				// put css files in assets/css or main stylesheet in root for wp themes
				return data.chunk.name === 'style' ? '../../[name].css' : '../css/[name].css';
			}
		}),
		new ESLintPlugin({
			files: 'assets/src/**/*.(js|jsx|ts|tsx)',
			extensions: ['.js', '.jsx', '.ts', '.tsx'],
			lintDirtyModulesOnly: true,
			emitError: true,
			emitWarning: false,
			failOnError: false,
			failOnWarning: false,
		})
	]
};
