# Nyheter
En samleplass for alle nyheter

## Krav
1. En meny med favuritt nyhetskilder.
2. Kunne velge en kilde og se alle artikklene sortert etter nyest først.
3. Må se moderne ut!

## Nyhetskilder:
- NRK `https://www.nrk.no/toppsaker.rss`
- VG `https://www.vg.no/rss/feed`
- TV2 `https://www.tv2.no/rest/cms-feeds-dw-rest/v2/cms/article/nyheter`
- Dagbladet `https://www.dagbladet.no/?lab_viewport=rss`
- Aftenposten `https://www.aftenposten.no/rss/`
- Dagsavisen `https://www.dagsavisen.no/arc/outboundfeeds/rss/`
- Nationen `https://www.nationen.no/?feed=tunrss`
- Finansavisen Artikler `https://ws.finansavisen.no/api/articles.rss`
- Finansavisen`https://www.finansavisen.no/info/rss-feed?zephr_sso_ott=bigV1s`
- Helsebiblioteket `https://www.helsebiblioteket.no/rss`
- BBC World `https://feeds.bbci.co.uk/news/world/rss.xml`
- BBC `http://feeds.bbci.co.uk/news/rss.xml`
- NewYorkTimes `https://www.nytimes.com/svc/collections/v1/publish/https:/www.nytimes.com/section/world/rss.xml`



# Oppsett
 - Klone filer med `git clone`
 - Kjør  `npm install`.
 - Kjør `npm start` for å kompilere js og sass/css
 - Kjør `npm run build` for å kompilere til ferdig produkt


# Oppgaver
Noe av filene er gjort klar med kommentarer. Se over filene før du begynner med oppgavene.


### Oppgave 1
Lag en array news_sources med alle nyhetslinker i.

### Oppgave 2
Lag en funksjon med navnet render_newssources() som skal loope over array lagd i punkt 1 og printe i sidebar.

### Oppgave 3
Gi sidebar og knappene en stil. Du står helt fritt til å velge farger, plasseringer osv.

### Oppgave 4
Lag en funksjon render_articles() som skal ta imot en nyhetskilde og printe ut
1. Logo eller tittel til avisen
2. Du skal kunne klikke på lenke å havne på nettsiden til avisen
3. Alle artikkler skal vises

### Extras
1. Print ut kategorier som hashtags
2. Vis aktiv nyhetskilde
3. Gjør aktiv nyhetskilde-knapp avrundet